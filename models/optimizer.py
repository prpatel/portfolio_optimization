# models/optimizer.py
import numpy as np
from scipy.optimize import minimize

class PortfolioOptimizer:
    def __init__(self, returns):
        self.returns = returns
        self.mean_returns = returns.mean()*252
        self.cov_matrix = returns.cov()*252

    def portfolio_performance(self, weights):
        returns = np.dot(weights, self.mean_returns)
        risk = np.sqrt(np.dot(weights.T, np.dot(self.cov_matrix, weights)))
        sharpe_ratio = (returns - 0.0454) / risk
        return returns, risk, sharpe_ratio

    def minimize_volatility(self):
        num_assets = len(self.mean_returns)
        args = (self.mean_returns, self.cov_matrix)
        constraints = ({'type': 'eq', 'fun': lambda x: np.sum(x) - 1})
        bounds = tuple((0, 1) for asset in range(num_assets))
        result = minimize(self.negative_sharpe_ratio, num_assets * [1. / num_assets,], args=args,
                          method='SLSQP', bounds=bounds, constraints=constraints)
        return result

    def negative_sharpe_ratio(self, weights, mean_returns, cov_matrix, risk_free_rate=0):
        returns = np.dot(weights, mean_returns)
        risk = np.sqrt(np.dot(weights.T, np.dot(cov_matrix, weights)))
        sharpe_ratio = (returns - risk_free_rate) / risk
        return -sharpe_ratio
