# main.py
import pandas as pd
from data.data_fetcher import DataFetcher
from models.optimizer import PortfolioOptimizer
from utils.plotting import plot_efficient_frontier
from utils.plotting import plot_cumulative_returns
from utils.plotting import plot_correlation_matrix

# Visualization to show how the sample portfolio would perform relative to the market

def main():
    tickers = ['AAPL', 'MSFT', 'GOOGL', 'AMZN', 'META']
    start_date = '2020-01-01'
    end_date = '2023-01-01'
    benchmark_ticker = 'SPY'  # S&P 500 ETF as benchmark

    data_fetcher = DataFetcher(tickers, start_date, end_date, benchmark_ticker)
    data = data_fetcher.fetch_data()
    returns = data_fetcher.calculate_returns(data)

    portfolio_returns = returns[tickers]
    benchmark_returns = returns[benchmark_ticker]
    
    plot_cumulative_returns(portfolio_returns, benchmark_returns)
    plot_correlation_matrix(returns)
    
    
    
    optimizer = PortfolioOptimizer(portfolio_returns)
    # minimize the volatility of the portfolio usinf scipy's minimize function to get the optimal weights
    min_vol_result = optimizer.minimize_volatility()
    #apply the optimal weights to the portfolio to get the optimal return, volatility, and Sharpe ratio
    optimal_portfolio_return, optimal_portfolio_volatility, optimal_portfolio_sharpe_ratio = optimizer.portfolio_performance(min_vol_result.x)
    
    #MC method for efficient frontier
    mc_weights, mc_volatility, mc_return = plot_efficient_frontier(portfolio_returns.mean(), portfolio_returns.cov())
    # apply the optimal weights to the portfolio to get the optimal return, volatility, and Sharpe ratio
    mc_portfolio_return, mc_portfolio_volatility, mc_portfolio_sharpe_ratio = optimizer.portfolio_performance(mc_weights)
    
    print(f'Optimal Portfolio Return: {optimal_portfolio_return}')
    print(f'Optimal Portfolio Volatility: {optimal_portfolio_volatility}')
    print(f'Optimal Portfolio Sharpe Ratio: {optimal_portfolio_sharpe_ratio}')
    
    print(f'MC Portfolio Return: {mc_portfolio_return}')
    print(f'MC Portfolio Volatility: {mc_portfolio_volatility}')
    print(f'MC Portfolio Sharpe Ratio: {mc_portfolio_sharpe_ratio}')
    

if __name__ == '__main__':
    main()

