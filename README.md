# Portfolio Optimization

This project demonstrates how to optimize a financial portfolio using historical stock data. The key steps include data fetching, processing, optimization, and visualization.

## Requirements

- Python 3.x
- Libraries: numpy, pandas, yfinance, matplotlib, scipy

## How to Run

1. Clone the repository:
   ```bash
   git clone https://gitlab.cern.ch/prpatel/portfolio_optimization.git
   cd portfolio_optimization
