# utils/plotting.py
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

def plot_efficient_frontier(returns, cov_matrix, num_portfolios=10000, risk_free_rate=0.0454):
    results = np.zeros((3, num_portfolios)) #array to store the returns, volatility, and Sharpe ratio of each portfolio
    weights_record = []

    for i in range(num_portfolios): # monte carlo simulation to generate random weights for each asset in the portfolio
        weights = np.random.random(returns.shape[0])
        weights /= np.sum(weights)
        portfolio_return = np.dot(weights, returns)*252
        portfolio_stddev = np.sqrt(np.dot(weights.T, np.dot(cov_matrix, weights)))*np.sqrt(252)
        results[0,i] = portfolio_return
        results[1,i] = portfolio_stddev
        results[2,i] = (portfolio_return - risk_free_rate) / portfolio_stddev #Sharpe ratio
        weights_record.append(weights)

    max_sharpe_idx = np.argmax(results[2]) #index of the portfolio with the highest Sharpe ratio
    sdp, rp = results[1,max_sharpe_idx], results[0,max_sharpe_idx] #standard deviation and return of the portfolio with the highest Sharpe ratio
    
    # weights of the portfolio with the highest Sharpe ratio
    optimal_weights = weights_record[max_sharpe_idx]
    
    plt.figure(figsize=(10, 7))
    plt.scatter(results[1,:], results[0,:]*100, c=results[2,:], cmap='YlGnBu', marker='o')
    plt.colorbar(label='Sharpe ratio')
    plt.scatter(sdp, rp * 100, marker='*', color='r', s=200)
    plt.xlabel('Volatility')
    plt.ylabel('Returns (%)')
    plt.title('Efficient Frontier')
    plt.savefig('efficient_frontier.png')
    
    return optimal_weights, sdp, rp

def plot_cumulative_returns(portfolio_returns, benchmark_returns):
    cumulative_portfolio_returns = portfolio_returns.add(1).cumprod().sub(1) * 100
    cumulative_benchmark_returns = benchmark_returns.add(1).cumprod().sub(1) * 100
    
    plt.figure(figsize=(10, 7))
    cumulative_portfolio_returns.plot(label='Portfolio Returns')
    cumulative_benchmark_returns.plot(label='Benchmark Returns', color='black', linestyle='-', linewidth=2)
    
    plt.title('Cumulative Returns')
    plt.xlabel('Date')
    plt.ylabel('Cumulative Returns (%)')
    plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
    
    plt.savefig('cumulative_returns.png', bbox_inches='tight')

def plot_correlation_matrix(data):
    corr = data.corr()
    plt.figure(figsize=(10, 7))
    sns.heatmap(corr, annot=True, cmap='coolwarm', cbar_kws={'label': 'Correlation'})
    plt.title('Correlation Matrix')
    plt.savefig('correlation_matrix.png')