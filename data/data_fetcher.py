# data/data_fetcher.py
import yfinance as yf
import pandas as pd
import matplotlib.pyplot as plt

class DataFetcher:
    def __init__(self, tickers, start_date, end_date, benchmark_ticker):
        self.tickers = tickers
        self.start_date = start_date
        self.end_date = end_date
        self.benchmark_ticker = benchmark_ticker

    def fetch_data(self):
        data = yf.download(self.tickers + [self.benchmark_ticker], start=self.start_date, end=self.end_date)
        return data['Adj Close']

    def calculate_returns(self, data):
        returns = data.pct_change().dropna()
        return returns
    
    def calculate_log_returns(self, data):
        log_returns = np.log(data / data.shift(1)).dropna()
        return log_returns
    
    